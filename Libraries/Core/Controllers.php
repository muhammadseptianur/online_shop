<?php

class Controllers
{
    public function __construct()
    {
        //INVOKE METHOD LoadModel() TO BE EXECUTED
        $this->LoadModel();
    
    }
    //LOAD MODEL CLASS
    public function LoadModel()
    {
        //GET THE NAME OF THE CONTROLLER CLASS AND CONCATENATE WITH WORD "Model"
        //TO GET MODEL OF CONTROLLER LIKE "homeModel"
        $model = get_class($this)."Model";

        //REFER TO THE MODEL FOLDER
        $routeClass = "Models/".$model.".php";

        //CHECK IF THE MODEL EXISTS
        //IF IT EXISTS CALL THE CLASS ONCE & INSTANCE THE MODEL
        if(file_exists($routeClass))
        {
            require_once($routeClass);
            $this->model= new $model();
        }
    
    }
}
